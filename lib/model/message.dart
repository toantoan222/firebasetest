import 'package:flutter/material.dart';

@immutable
class Noti {
  final String id;
  final String title;
  final String body;

  const Noti({
    @required this.id,
    @required this.title,
    @required this.body,
  });
}
